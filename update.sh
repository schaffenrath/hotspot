#!/bin/bash

CWD=$(pwd)
DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)

# migrate database
echo "Migration database ..."
python3 manage.py makemigrations hotspot
python3 manage.py migrate

# update static files
echo "Updating bower packages ..."
cd $DIR/hotspot/static
bower install

cd $CWD
