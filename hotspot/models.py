import os
import uuid

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django import forms
from django.contrib import admin

from . import settings

class HotspotModel(models.Model):
	uuid     = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	name     = models.CharField(max_length=50, default="", blank=True, null=False)
	desc     = models.CharField(max_length=256, default="", blank=True, null=False)
	geo_lat  = models.DecimalField(default=0, max_digits=10, decimal_places=4)
	geo_long = models.DecimalField(default=0, max_digits=10, decimal_places=4)
	
	score    = models.PositiveIntegerField(default=0)     # from 0 to n
	plausibility = models.PositiveIntegerField(default=0) # from 0 to 100

	# idea: update_counter, number of times the score got updated. Or date?
	# older hotspots are in general more plausible than new ones. Or not?

	def __str__(self):
		return "Hotspot[" + str(self.uuid) + "|" + str(self.name) + "|" + str(self.score) + "]"

admin.site.register(HotspotModel)