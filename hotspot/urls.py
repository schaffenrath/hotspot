from django.conf.urls import url
from django.contrib import admin

from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    
    url(r'^$', views.index),
    url(r'^api/hotspots/$', views.api_hotspots), # returns hotspots sorted by score
]
