from rest_framework import serializers

from . import models

class HotspotSerializer(serializers.Serializer):
	uuid  = serializers.UUIDField()
	
	name  = serializers.CharField(max_length=128)
	desc  = serializers.CharField(max_length=256)

	geo_lat  = serializers.DecimalField(max_digits=10, decimal_places=4)
	geo_long = serializers.DecimalField(max_digits=10, decimal_places=4)
	
	score = serializers.IntegerField()
	plausibility = serializers.IntegerField()