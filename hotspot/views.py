import os
import threading
import time

from django import http, template
from django.http import HttpResponse, JsonResponse

from django.template.response import TemplateResponse
from django.template import TemplateDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from . import models, serializers

# view functions

def index(request):
	return TemplateResponse(request, "index.html")

# this function returns the top 10 hotspots sorted by 'score'
def api_hotspots(request):
	hotspots = reversed(models.HotspotModel.objects.order_by("score")[:10])
	return HttpResponse(status=200, content_type="application/json",
		content=JSONRenderer().render(serializers.HotspotSerializer(hotspots, many=True).data))

# thread fetches every 2 hours data and
# updates the database
def fetch_data_loop():
	while(True):
		#print("[FETCH_THREAD] started")

		# add some test data for testing

		if models.HotspotModel.objects.filter(name="Innsbruck").first() is None:
			innsbruck = models.HotspotModel()
			innsbruck.name = "Innsbruck"
			innsbruck.desc = "Schönste Stadt der Welt <3"
			innsbruck.geo_lat  = 47.2692
			innsbruck.geo_long = 11.4041
			innsbruck.save()

		if models.HotspotModel.objects.filter(name="UIBK").first() is None:
			uibk = models.HotspotModel()
			uibk.name = "UIBK"
			uibk.desc = "Schönste UNI der Welt <3"
			uibk.geo_lat  = 47.2641
			uibk.geo_long = 11.3438
			uibk.save()

		if models.HotspotModel.objects.filter(name="Alins Zuhause").first() is None:
			alin = models.HotspotModel()
			alin.name = "Alins Zuhause"
			alin.desc = "Hier geht die Party ab! Nur C-Programmierer willkommen!"
			alin.geo_lat  = 47.2627
			alin.geo_long = 11.3778
			alin.score = 1000
			alin.plausibility = 100
			alin.save()

		#print("[FETCH_THREAD] stopped")

		time.sleep(120 * 60) # 2 hours

t = threading.Thread(target=fetch_data_loop)
t.setDaemon(True)
t.start()
