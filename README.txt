You need:

 - python3
 - pip3
 - django
 - django rest
 - bower
 - facebook sdk

Install django, django-rest using pip3: 

pip3 install django
pip3 install djangorestframework
pip3 install markdown       
pip3 install django-filter
pip3 install facebook-sdk

Run web application: 

./start.sh

Application running on 0.0.0.0:8000

Update web application: 

./update.sh

Admin user:

root:Master123
